﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] GameObject player;
    Vector3 playerPosition;
    Vector3 startPosition;
    float speed = 10f;
    [SerializeField] GameObject explosionPrefab;
    AudioSource audiosrc;
    GameObject boomSpot;
    CircleCollider2D circleCol;
    bool hasExploded = false;
    void Start()
    {
        circleCol = GetComponent<CircleCollider2D>();
        playerPosition = player.transform.position;
        audiosrc = GetComponent<AudioSource>();
        
        circleCol.isTrigger = false;
    }


    void Update()
    {


        float step = speed * Time.deltaTime; // calculate distance to move


        transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, -3f, 0f), step);
        if (transform.position.y == -3f)
        {
            if (hasExploded == false)
            {
                StartCoroutine(Explode());
                hasExploded = true;
            }

        }
    }

    IEnumerator Explode()
    {
        audiosrc.Play();
        GameObject boom = Instantiate(explosionPrefab, playerPosition, transform.rotation);
        boom.transform.localScale = new Vector3(12f, 12f, 1f);
        circleCol.radius = 19f;
        circleCol.isTrigger = true;
        yield return new WaitForSeconds(1);
        Destroy(boom, 0.4f);
        Destroy(boomSpot);
        Destroy(gameObject);
    }

    void MarkExplosionSpot()
    {
        boomSpot = Instantiate(explosionPrefab, playerPosition, transform.rotation);
    }

}
