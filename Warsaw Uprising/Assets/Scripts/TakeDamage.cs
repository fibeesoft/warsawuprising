﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakeDamage : MonoBehaviour
{
    int hpMax, hp;
    [SerializeField] GameObject ammoBoxPrefab;
    [SerializeField] Text txt_hp;
    [SerializeField] Slider slider_hp;
    [SerializeField] Image Fill;
    public Color MaxHealthColor = Color.green;
    public Color MinHealthColor = Color.red;
    Animator anim;

    private void Awake()
    {
      
    }
    private void Start()
    {
        if (gameObject.GetComponent<Animator>() != null)
        {
            anim = GetComponent<Animator>();
        }

        if (gameObject.transform.tag == "Player")
        {
            hpMax = 10;
            hp = hpMax;
        }else if(gameObject.transform.tag == "Enemy")
        {
            hpMax = 8;
            hp = hpMax;
        }
        slider_hp.maxValue = hp;
        slider_hp.value = hp;
        txt_hp.text = hp.ToString();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "bullet")
        {
            TakeADamage(2);
        }
        else if(collision.gameObject.tag == "bulletHandgun")
        {
            TakeADamage(1);
        }
        else if(collision.gameObject.tag == "bomb")
        {
            TakeADamage(6);
        }

    }

    void TakeADamage(int damage)
    {
        if(hp > damage)
        {
            hp -= damage;
            txt_hp.text = hp.ToString();
            slider_hp.value = hp;
            ChangeSliderColor();
            if(gameObject.transform.tag == "Player"){
                GameController.instance.ShowHitScreenCoroutine();
            }
        }
        else
        {
            slider_hp.value = 0;
            EnemyDie();
        }
    }

    void EnemyDie()
    {
        if(gameObject.transform.tag == "Enemy")
        {
            Destroy(gameObject);
            GameObject ammoBox = Instantiate(ammoBoxPrefab, transform.position, transform.rotation);
            GameController.instance.AddKilled();
        }else if(gameObject.transform.tag == "Player")
        {
            anim.Play("die");
            GameController.instance.GameOn(false);
            GameController.instance.ShowGameOverPanel();
        }

    }

    void ChangeSliderColor()
    {
        Fill.color = Color.Lerp(MinHealthColor, MaxHealthColor, (float)hp / hpMax);
    }
}
