﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    [SerializeField] GameObject bulletPrefab, bulletPrefabHandgun, rightArm, aimPos, playerSprites;
    [SerializeField] Text txt_ammoQuantity;
    float movingSpeed, movingYSpeed;
    float shootingSpeed = 3000f;
    Camera cam;
    int ammoQuantity = 30;
    public bool isAlive;
    [SerializeField] Sprite sprite_handgun, sprite_submachine;
    [SerializeField] SpriteRenderer img_gun;
    int weaponChoosen = 1;
    AudioSource audiosrc;
    [SerializeField] AudioClip clipGun, clipSubmachine;
    Animator anim;


    void Start()
    {
        isAlive = true;
        audiosrc = GetComponent<AudioSource>();
        DisplayAmmoCounter();
        cam = Camera.main;
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (GameController.instance.CheckIfGameIsOn())
        {
            RotateObjectToMousePosition();
            movingSpeed = Input.GetAxis("Horizontal");
            movingYSpeed = Input.GetAxis("Vertical");
            Move();
            
            cam.transform.position = new Vector3(transform.position.x + 5f, transform.position.y, -10);
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
                anim.Play("shooting");
            }
        }
    }
    public void Move()
    {
        Vector3 pos = transform.position;
        pos.x = pos.x + movingSpeed*10*Time.deltaTime;
        pos.y = pos.y + movingYSpeed * 10 * Time.deltaTime;

        transform.position = new Vector3(Mathf.Clamp(pos.x, -7f, 50f), Mathf.Clamp(pos.y, -6f, -0.5f), 0f);

        if(movingSpeed == 0 && movingYSpeed == 0)
        {
            anim.Play("idle");
        }
        else
        {
            anim.Play("running");
        }
    }

    void RotateObjectToMousePosition()
    {
        Vector3 mousePosOnTheScreen = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosOnTheScreen.z = 0f;
        Vector3 direction = new Vector3(
            mousePosOnTheScreen.x - rightArm.transform.position.x,
            mousePosOnTheScreen.y - rightArm.transform.position.y,
            0
            );

        Vector3 dir = mousePosOnTheScreen - rightArm.transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        rightArm.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        float orientation = mousePosOnTheScreen.x - transform.position.x;

        if (orientation >= 0f)
        {
            playerSprites.transform.localEulerAngles = new Vector3(0, 0, 0);
            rightArm.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            playerSprites.transform.localEulerAngles = new Vector3(0f, 180f, 0);
            rightArm.transform.localScale = new Vector3(1f, -1f, 1f);
        }
    }


    void Shoot()
    {
        if(ammoQuantity > 0)
        {
            SwitchToSubmachine();
            audiosrc.clip = clipSubmachine;
            audiosrc.Play();
            GameObject bullet = Instantiate(bulletPrefab, aimPos.transform.position, aimPos.transform.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = rightArm.transform.TransformDirection(new Vector2
                (
                 (Mathf.Cos(aimPos.transform.rotation.z * Mathf.Deg2Rad) * shootingSpeed * Time.deltaTime),
                 (Mathf.Sin(aimPos.transform.rotation.z * Mathf.Deg2Rad) * shootingSpeed * Time.deltaTime))
                );
            Destroy(bullet, 3);
            ammoQuantity--;
            DisplayAmmoCounter();
        }
        else
        {
            SwitchToPistol();
            audiosrc.clip = clipGun;
            audiosrc.Play();
            GameObject bullet = Instantiate(bulletPrefabHandgun, aimPos.transform.position, aimPos.transform.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = rightArm.transform.TransformDirection(new Vector2
                (
                 (Mathf.Cos(aimPos.transform.rotation.z * Mathf.Deg2Rad) * shootingSpeed /2 * Time.deltaTime),
                 (Mathf.Sin(aimPos.transform.rotation.z * Mathf.Deg2Rad) * shootingSpeed /2 * Time.deltaTime))
                );
            Destroy(bullet, 1);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "ammoBox")
        {
            ammoQuantity += 10;
            DisplayAmmoCounter();
        }
    }

    void DisplayAmmoCounter()
    {
        if (GameController.instance.isGameOn == true)
        {
            txt_ammoQuantity.text = ammoQuantity.ToString();
        }
    }

    void SwitchToPistol()
    {
        weaponChoosen = 0;
        img_gun.sprite = sprite_handgun;
    }

    void SwitchToSubmachine()
    {
        weaponChoosen = 1;
        img_gun.sprite = sprite_submachine;
    }
}