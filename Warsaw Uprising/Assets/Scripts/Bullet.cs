﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] GameObject hitPrefab;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision.gameObject.tag != "ammoBox") && (gameObject.tag != "bulletGerman" || collision.tag != "Enemy"))
        {
            InstantiateBulletHit(collision);
            Destroy(gameObject);
        }

    }

    void InstantiateBulletHit(Collider2D collision)
    {
        GameObject bulletHit = Instantiate(hitPrefab, transform.position, transform.rotation);
        Destroy(bulletHit, 0.3f);
    }
}
