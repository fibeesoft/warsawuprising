﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] Transform enemy;
    [SerializeField] GameObject bulletPrefab, arm, aimPos, player;
    float bulletSpeed = 2000f;
    int shootingSpeed = 1;
    [Range(10, 100)] public int ammoQuantity = 10;
    bool isShooting = false;
    [SerializeField] SpriteRenderer img_gun;
    [SerializeField] Sprite sprite_handgun;
    [SerializeField] Sprite sprite_submachine;
    bool isReady = false;
    AudioSource audiosrc;
    [SerializeField] AudioClip clipGun, clipSubmachine;
    [SerializeField] GameObject enemySprites;
    private void Start()
    {
        img_gun.sprite = sprite_submachine;
        audiosrc = GetComponent<AudioSource>();
        StartCoroutine(GetReady());
    }

    IEnumerator GetReady()
    {
        yield return new WaitForSeconds(3);
        isReady = true;
    }
    void Update()
    {
        if (GameController.instance.CheckIfGameIsOn() && isReady)
        {
            RotateObjectToPlayerPosition();
            if (isShooting == false)
            {
                StartCoroutine(Shoot());
                isShooting = true;
            }
        }
        else
        {
            StopCoroutine(Shoot());

        }
    }
    

    void RotateObjectToPlayerPosition()
    {
        float playerPos = enemy.position.x - transform.position.x;

        if(playerPos <= 0f)
        {
            enemySprites.transform.localEulerAngles = new Vector3(0f, 180f, 0);
            arm.transform.localScale = new Vector3(1f, -1f, 1f);
        }
        else
        {
            enemySprites.transform.localEulerAngles = new Vector3(0f, 0, 0);
            arm.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        Vector3 dir = enemy.position - arm.transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        arm.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    IEnumerator Shoot()
    {
        if(ammoQuantity > 0)
        {
            SwitchToSubmachineGun();
            ammoQuantity--;
            float randNumber = Random.Range(-6f, 6f);
            GameObject bullet = Instantiate(bulletPrefab, aimPos.transform.position, aimPos.transform.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = arm.transform.TransformDirection(new Vector2
                (
                 (Mathf.Cos(aimPos.transform.rotation.z * Mathf.Deg2Rad) * bulletSpeed * Time.deltaTime),
                 (Mathf.Sin(aimPos.transform.rotation.z * Mathf.Deg2Rad * randNumber) * bulletSpeed * Time.deltaTime))
                );
            Destroy(bullet, 3);
            print("shooting");
            yield return new WaitForSeconds(shootingSpeed);
            StartCoroutine(Shoot());
        }
        else
        {

            SwitchToHandGun();

            float randNumber = Random.Range(-6f, 6f);
            GameObject bullet = Instantiate(bulletPrefab, aimPos.transform.position, aimPos.transform.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = arm.transform.TransformDirection(new Vector2
                (
                 (Mathf.Cos(aimPos.transform.rotation.z * Mathf.Deg2Rad) * bulletSpeed * Time.deltaTime),
                 (Mathf.Sin(aimPos.transform.rotation.z * Mathf.Deg2Rad * randNumber) * bulletSpeed * Time.deltaTime))
                );
            Destroy(bullet, 1);
            print("shooting");
            yield return new WaitForSeconds(shootingSpeed);
            StartCoroutine(Shoot());
        }

    }

    void SwitchToHandGun()
    {
        img_gun.sprite = sprite_handgun;
        shootingSpeed = 3;
        bulletSpeed = 1000;
        audiosrc.clip = clipGun;
        audiosrc.Play();
    }

    void SwitchToSubmachineGun()
    {
        img_gun.sprite = sprite_submachine;
        bulletSpeed = 2000;
        shootingSpeed = 1;
        audiosrc.clip = clipSubmachine;
        audiosrc.Play();
    }


}
