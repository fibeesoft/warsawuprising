﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] GameObject gameOverPanel, hitScreen, crosshair;
    [SerializeField] Text txt_killed;
    public static GameController instance;
    public bool isGameOn = true;
    int howManyKilled = 0;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        hitScreen.SetActive(false);
        Time.timeScale = 1.0f;
        
    }

    private void Update()
    {
        Vector3 mousePosOnTheScreen = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosOnTheScreen.z = 0f;
        if (isGameOn)
        {
            crosshair.SetActive(true);
            crosshair.transform.position = mousePosOnTheScreen;
            Cursor.visible = false;

        }
        else
        {
            crosshair.SetActive(false);
            Cursor.visible = true;
        }
       
    }

    public void ShowHitScreenCoroutine()
    {
        StartCoroutine(ShowHitScreen());
    }

    IEnumerator ShowHitScreen()
    {
        hitScreen.SetActive(true);
        yield return new WaitForSeconds(2);
        hitScreen.SetActive(false);
    }

    public bool CheckIfGameIsOn()
    {
        return isGameOn;
    }

    public void GameOn(bool isGameOn_p)
    {
        isGameOn = isGameOn_p;

    }
    public void GoToSettingsPanel()
    {
        SceneManager.LoadScene(0);
        gameOverPanel.SetActive(false);
        Time.timeScale = 0f;
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameOverPanel.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void ShowGameOverPanel()
    {
        isGameOn = false;
        gameOverPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void AddKilled()
    {
        howManyKilled++;
        txt_killed.text = howManyKilled.ToString();
    }

}
