﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombDropper : MonoBehaviour
{
    [SerializeField] GameObject bombPrefab, player;
    bool wasBombDropped = false;
    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            print("bomb dropped");
            wasBombDropped = true;
            transform.position = new Vector3(player.transform.position.x, 7f, 0f);
            GameObject bomb2 = Instantiate(bombPrefab, transform.position, transform.rotation);
        }
    }
}
